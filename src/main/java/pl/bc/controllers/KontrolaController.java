package pl.bc.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.bc.entity.Kontrola;
import pl.bc.entity.ListPomocnicza;
import pl.bc.entity.Oddzial;
import pl.bc.entity.Pracownik;
import pl.bc.entity.PrzypadekKontrolny;
import pl.bc.entity.RodzajKontroli;
import pl.bc.entity.TestKontrolny;
import pl.bc.entity.WynikKontroli;
import pl.bc.entity.WynikList;
import pl.bc.entity.WynikTestu;
import pl.bc.repository.KontrolaRepository;
import pl.bc.repository.OddzialRepository;
import pl.bc.repository.PracownikRepository;
import pl.bc.repository.PrzypadekKontrolnyRepository;
import pl.bc.repository.RodzajKontroliRepository;
import pl.bc.repository.TestKontrolnyRepository;
import pl.bc.repository.WynikKontroliRepository;
import pl.bc.repository.WynikTestuRepository;
import pl.bc.services.KontrolaService;

@Controller
@Component
public class KontrolaController {

	@Autowired
	KontrolaRepository kontrolaRepository;
	@Autowired
	PrzypadekKontrolnyRepository przypadekRepository;
	@Autowired
	TestKontrolnyRepository testKontrolnyRepository;
	@Autowired
	KontrolaService kontrolaService;
	@Autowired
	RodzajKontroliRepository rodzajKontroliRepository;
	@Autowired
	OddzialRepository oddzialRepository;
	@Autowired
	PracownikRepository pracownikRepository;
	@Autowired
	WynikTestuRepository wynikTestuRepository;
	@Autowired
	WynikKontroliRepository wynikKontroliRepository;

	@RequestMapping("/kontrole")
	public String wszystkieKontrolne(Model model) {
		List<Kontrola> kontrole = kontrolaService.getKontrole();
		model.addAttribute("kontrole", kontrole);
		return "kontrole/wszystkieKontrole";
	}

	@RequestMapping("/kontrole/{id}")
	public String podgladKontroli(Model model, @PathVariable("id") Long id) {
		Optional<Kontrola> kontrola = kontrolaService.getKontrolaById(id);
		kontrola.ifPresent(o -> model.addAttribute("kontr", o));

		List<PrzypadekKontrolny> przypadki = przypadekRepository.findPrzypadkiByKontrola(id);
		model.addAttribute("przypadki", przypadki);

		List<TestKontrolny> testy = testKontrolnyRepository.getTestyKontrolneByIdKontroli(id);
		model.addAttribute("testy", testy);

		return "kontrole/podgladKontroli";
	}


	@RequestMapping(value = { "/kontrole/dodajKontrole"})
	public String nowaKontrola(Model model) {
		model.addAttribute("kontrola", new Kontrola());
		List<RodzajKontroli> rodzajeKontroli = rodzajKontroliRepository.findByCzyAktywnaTrue();
		model.addAttribute("rodzajeKontroli", rodzajeKontroli);
		List<Oddzial> oddzialy = oddzialRepository.findAll();
		model.addAttribute("oddzialy", oddzialy);
		return "kontrole/dodajKontrole";
	}

	@RequestMapping(value = "/kontroleDodaj", method = RequestMethod.POST)
	public String dodajKontrole(Model model, @ModelAttribute @Valid Kontrola nowaKontrola, Errors errors,
			@RequestParam Long rodzajKontroliId, @RequestParam int wybranyOddzial, @RequestParam String date) {
		if (errors.hasErrors()) {
			model.addAttribute("kontrola", nowaKontrola);
			return "kontrole/dodajKontrole";
		}
		RodzajKontroli rodzaj = rodzajKontroliRepository.getOne(rodzajKontroliId);
		Oddzial oddzial = oddzialRepository.getOne(wybranyOddzial);
		List<Pracownik> pracownicy = pracownikRepository.findByOddzial(oddzial);
		nowaKontrola.setRodzajKontroli(rodzaj);
		nowaKontrola.setOddzial(oddzial);
		nowaKontrola.setTerminKontroli(getAsDate(date));
		kontrolaRepository.save(nowaKontrola);
		for (Pracownik pracownik : pracownicy) {
			PrzypadekKontrolny przypadekKontrolny = new PrzypadekKontrolny(nowaKontrola, pracownik);
			przypadekKontrolny.setPracownik(pracownik);
			przypadekKontrolny.setKontrola(nowaKontrola);
			przypadekRepository.save(przypadekKontrolny);
		}
		return "redirect:/kontrole";
	}

	@RequestMapping(value = "/kontrole/wykonajKontrole/{id}")
	public String editKontrola(Model model, @PathVariable("id") Long id) {
		Kontrola kontrola = kontrolaRepository.getOne(id);
		model.addAttribute("kontrola", kontrola);

		List<PrzypadekKontrolny> przypadki = przypadekRepository.findPrzypadkiByKontrola(id);
		model.addAttribute("przypadki", przypadki);

		List<TestKontrolny> testy = testKontrolnyRepository.getTestyKontrolneByIdKontroli(id);
		model.addAttribute("testy", testy);

		List<WynikTestu> odpowiedzi = wynikTestuRepository.findAll();
		model.addAttribute("odpowiedzi", odpowiedzi);

		ArrayList<WynikList> wynik = new ArrayList<WynikList>();
		for (PrzypadekKontrolny przypadek : przypadki) {
			for (TestKontrolny test : testy) {
				wynik.add(new WynikList(przypadek.getId(), przypadek.getPracownik().getNRP(),
						przypadek.getPracownik().getImie(), przypadek.getPracownik().getNazwisko(), test.getId(), 0L));
			}
		}
		ListPomocnicza wrapper = new ListPomocnicza();
		wrapper.setListaPomocnicza(wynik);
		model.addAttribute("wrapper", wrapper);

		return "kontrole/wykonajKontrole";
	}
	
	
	@RequestMapping(value = "/wykonajKontrole", method= {RequestMethod.POST})
	public String wykonajKontrole(Model model, @ModelAttribute ListPomocnicza wrapper) {
		
		System.out.println(wrapper.getListaPomocnicza() !=null ? wrapper.getListaPomocnicza().size(): "null list");
		
		model.addAttribute("wrapper", wrapper);
		
	
		return "redirect:/kontrole";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	private static Date getAsDate(String dateString) {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {
			date = df.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

}

package pl.bc.entity;

import java.util.ArrayList;


public class WynikList {
	private Long IdPrzypadku;
	private Long NRP;
	private String Imie;
	private String Nazwisko;
	private Long IdTestu;
	private Long IdWynikuTestu;
	
	private WynikList(){
	}

	public WynikList(Long idPrzypadku, Long nRP, String imie, String nazwisko, Long idTestu, Long idWynikuTestu) {
		this.IdPrzypadku = idPrzypadku;
		this.NRP = nRP;
		this.Imie = imie;
		this.Nazwisko = nazwisko;
		this.IdTestu = idTestu;
		this.IdWynikuTestu = idWynikuTestu;
	}

	public Long getIdPrzypadku() {
		return IdPrzypadku;
	}

	public void setIdPrzypadku(Long idPrzypadku) {
		IdPrzypadku = idPrzypadku;
	}

	public Long getNRP() {
		return NRP;
	}

	public void setNRP(Long nRP) {
		NRP = nRP;
	}

	public String getImie() {
		return Imie;
	}

	public void setImie(String imie) {
		Imie = imie;
	}

	public String getNazwisko() {
		return Nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		Nazwisko = nazwisko;
	}

	public Long getIdTestu() {
		return IdTestu;
	}

	public void setIdTestu(Long idTestu) {
		IdTestu = idTestu;
	}

	public Long getIdWynikuTestu() {
		return IdWynikuTestu;
	}

	public void setIdWynikuTestu(Long idWynikuTestu) {
		IdWynikuTestu = idWynikuTestu;
	}
}

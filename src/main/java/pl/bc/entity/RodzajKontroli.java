package pl.bc.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class RodzajKontroli {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rodzaj_seq_gen")
	@SequenceGenerator(name = "rodzaj_seq_gen", sequenceName = "RODZAJ_SEQ")
	private long id;
	private String nazwa;
	private boolean czyAktywna;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataUruchomienia;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = true)
	private Date dataWycofania;

	@OneToMany(mappedBy = "rodzajKontroli")
	private List<Kontrola> kontrola;

	@OneToMany(mappedBy = "rodzajKontroli")
	private List<TestKontrolny> testKontrony;
	
	public RodzajKontroli() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public boolean isCzyAktywna() {
		return czyAktywna;
	}

	public void setCzyAktywna(boolean czyAktywna) {
		this.czyAktywna = czyAktywna;
	}

	public Date getDataUruchomienia() {
		return dataUruchomienia;
	}

	public void setDataUruchomienia(Date dataUruchomienia) {
		this.dataUruchomienia = dataUruchomienia;
	}

	public Date getDataWycofania() {
		return dataWycofania;
	}

	public void setDataWycofania(Date dataWycofania) {
		this.dataWycofania = dataWycofania;
	}

	public List<Kontrola> getKontrola() {
		return kontrola;
	}

	public void setKontrola(List<Kontrola> kontrola) {
		this.kontrola = kontrola;
	}

	public List<TestKontrolny> getTestKontrony() {
		return testKontrony;
	}

	public void setTestKontrony(List<TestKontrolny> testKontrony) {
		this.testKontrony = testKontrony;
	}

}

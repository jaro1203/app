package pl.bc.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class PrzypadekKontrolny {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "przypadek_seq_gen")
	@SequenceGenerator(name = "przypadek_seq_gen", sequenceName = "PRZYPADEK_SEQ")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "kontrola_id")
	private Kontrola kontrola;

	@ManyToOne
	@JoinColumn(name = "NRP")
	private Pracownik pracownik;

	@OneToMany(mappedBy = "przypadekKontrolny")
	private List<WynikKontroli> wynikKontroli;
	
	public PrzypadekKontrolny() {
			
	}
	public PrzypadekKontrolny(Kontrola kontrola, Pracownik pracownik) {
		this.kontrola=kontrola;
		this.pracownik=pracownik;
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Kontrola getKontrola() {
		return kontrola;
	}

	public void setKontrola(Kontrola kontrola) {
		this.kontrola = kontrola;
	}

	public Pracownik getPracownik() {
		return pracownik;
	}

	public void setPracownik(Pracownik pracownik) {
		this.pracownik = pracownik;
	}

	public List<WynikKontroli> getWynikKontroli() {
		return wynikKontroli;
	}

	public void setWynikKontroli(List<WynikKontroli> wynikKontroli) {
		this.wynikKontroli = wynikKontroli;
	}

}

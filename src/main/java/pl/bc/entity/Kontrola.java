package pl.bc.entity;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Kontrola {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kontrola_seq_gen")
	@SequenceGenerator(name = "kontrola_seq_gen", sequenceName = "KONTROLA_SEQ")
	private Long id;

	@OneToMany(mappedBy = "kontrola")
	private List<PrzypadekKontrolny> przypadekKontrolny;

	@ManyToOne
	@JoinColumn(name = "rodzajKontroli_id")
	private RodzajKontroli rodzajKontroli;

	@ManyToOne
	@JoinColumn(name = "nro")
	private Oddzial oddzial;

	@Temporal(TemporalType.TIMESTAMP)
	private Date terminKontroli;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = true)
	private Date dataKontroli;

	private boolean status;
	
	public Kontrola () {
	
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<PrzypadekKontrolny> getPrzypadekKontrolny() {
		return przypadekKontrolny;
	}

	public void setPrzypadekKontrolny(List<PrzypadekKontrolny> przypadekKontrolny) {
		this.przypadekKontrolny = przypadekKontrolny;
	}

	public RodzajKontroli getRodzajKontroli() {
		return rodzajKontroli;
	}

	public void setRodzajKontroli(RodzajKontroli rodzaj) {
		this.rodzajKontroli = rodzaj;
	}

	public Oddzial getOddzial() {
		return oddzial;
	}

	public void setOddzial(Oddzial oddzial) {
		this.oddzial = oddzial;
	}

	public Date getTerminKontroli() {
		return terminKontroli;
	}

	public void setTerminKontroli(Date terminKontroli) {
		this.terminKontroli = terminKontroli;
	}

	public Date getDataKontroli() {
		return dataKontroli;
	}

	public void setDataKontroli(Date dataKontroli) {
		this.dataKontroli = dataKontroli;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}

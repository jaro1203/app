package pl.bc.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class WynikTestu {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wynikTestu_seq_gen")
	@SequenceGenerator(name = "wynikTestu_seq_gen", sequenceName = "WYNIKTESTU_SEQ")
	private Long id;

	@ManyToMany
	@JoinTable(name = "test_wynik", joinColumns = { @JoinColumn(name = "wynikTestu_id") }, inverseJoinColumns = {
			@JoinColumn(name = "testKontrolny_id") })
	private List<TestKontrolny> testKontrolny;
	private String odpowiedz;

	@OneToMany(mappedBy = "wynikTestu")
	private List<WynikKontroli> wynikKontroli;
	
	public WynikTestu() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<TestKontrolny> getTestKontrolny() {
		return testKontrolny;
	}

	public void setTestKontrolny(List<TestKontrolny> testKontrolny) {
		this.testKontrolny = testKontrolny;
	}

	public String getOdpowiedz() {
		return odpowiedz;
	}

	public void setOdpowiedz(String odpowiedz) {
		this.odpowiedz = odpowiedz;
	}

	public List<WynikKontroli> getWynikKontroli() {
		return wynikKontroli;
	}

	public void setWynikKontroli(List<WynikKontroli> wynikKontroli) {
		this.wynikKontroli = wynikKontroli;
	}

}

package pl.bc.entity;

import java.util.ArrayList;
import java.util.List;

public class ListPomocnicza {
	
	private ArrayList<WynikList> listaPomocnicza;

	public ArrayList<WynikList> getListaPomocnicza() {
		return listaPomocnicza;
	}

	public void setListaPomocnicza(ArrayList<WynikList> wyn) {
		this.listaPomocnicza = wyn;
	}
	
}

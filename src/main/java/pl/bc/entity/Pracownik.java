package pl.bc.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Pracownik {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pracownik_seq_gen")
	@SequenceGenerator(name = "pracownik_seq_gen", sequenceName = "PRACOWNIK_SEQ", initialValue = 10000, allocationSize = 1)
	private Long NRP;

	@ManyToOne
	@JoinColumn(name = "NRO")
	private Oddzial oddzial;

	private String imie;
	private String nazwisko;
	private String email;
	private long idStanowiska; // relacja enum

	@OneToMany(mappedBy = "pracownik")
	private List<PrzypadekKontrolny> przypadekKontrolny;
	
	public Pracownik() {
		
	}

	public Long getNRP() {
		return NRP;
	}

	public void setNRP(Long nRP) {
		NRP = nRP;
	}

	public Oddzial getOddzial() {
		return oddzial;
	}

	public void setOddzial(Oddzial oddzial) {
		this.oddzial = oddzial;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getIdStanowiska() {
		return idStanowiska;
	}

	public void setIdStanowiska(long idStanowiska) {
		this.idStanowiska = idStanowiska;
	}

	public List<PrzypadekKontrolny> getPrzypadekKontrolny() {
		return przypadekKontrolny;
	}

	public void setPrzypadekKontrolny(List<PrzypadekKontrolny> przypadekKontrolny) {
		this.przypadekKontrolny = przypadekKontrolny;
	}

}

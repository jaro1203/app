package pl.bc.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class TestKontrolny {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "test_seq_gen")
	@SequenceGenerator(name = "test_seq_gen", sequenceName = "TEST_SEQ")
	private Long id;

	@ManyToMany(mappedBy = "testKontrolny")
	private List<WynikTestu> wynikTestu;

	@ManyToOne
	@JoinColumn(name = "rodzajKontroli_id")
	private RodzajKontroli rodzajKontroli;

	@OneToMany(mappedBy = "testKontrolny")
	private List<WynikKontroli> wynikKontroli;

	private String trescTestu;
	private boolean czyAktywny;
	
	public TestKontrolny() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<WynikTestu> getWynikTestu() {
		return wynikTestu;
	}
	public void setWynikTestu(List<WynikTestu> wynikTestu) {
		this.wynikTestu = wynikTestu;
	}
	public RodzajKontroli getRodzajKontroli() {
		return rodzajKontroli;
	}
	public void setRodzajKontroli(RodzajKontroli rodzajKontroli) {
		this.rodzajKontroli = rodzajKontroli;
	}
	public List<WynikKontroli> getWynikKontroli() {
		return wynikKontroli;
	}
	public void setWynikKontroli(List<WynikKontroli> wynikKontroli) {
		this.wynikKontroli = wynikKontroli;
	}
	public String getTrescTestu() {
		return trescTestu;
	}
	public void setTrescTestu(String trescTestu) {
		this.trescTestu = trescTestu;
	}
	public boolean isCzyAktywny() {
		return czyAktywny;
	}
	public void setCzyAktywny(boolean czyAktywny) {
		this.czyAktywny = czyAktywny;
	}

	
}

package pl.bc.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Oddzial {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "oddzial_seq_gen")
	@SequenceGenerator(name = "oddzial_seq_gen", sequenceName = "ODDZIAL_SEQ", initialValue = 1, allocationSize = 1)
	private int NRO;
	private String nazwa;
	private String ulica;
	private String kodPocztowy;
	private String miejscowosc;
	private String email;
	private boolean czyAktywny;

	@OneToMany(mappedBy = "oddzial")
	private List<Pracownik> pracownik;
	
	@OneToMany(mappedBy = "oddzial")
	private List<Kontrola> kontrola;
	
	public Oddzial() {
		
	}

	public int getNRO() {
		return NRO;
	}

	public void setNRO(int nRO) {
		NRO = nRO;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getKodPocztowy() {
		return kodPocztowy;
	}

	public void setKodPocztowy(String kodPocztowy) {
		this.kodPocztowy = kodPocztowy;
	}

	public String getMiejscowosc() {
		return miejscowosc;
	}

	public void setMiejscowosc(String miejscowosc) {
		this.miejscowosc = miejscowosc;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isCzyAktywny() {
		return czyAktywny;
	}

	public void setCzyAktywny(boolean czyAktywny) {
		this.czyAktywny = czyAktywny;
	}

	public List<Pracownik> getPracownik() {
		return pracownik;
	}

	public void setPracownik(List<Pracownik> pracownik) {
		this.pracownik = pracownik;
	}

	public List<Kontrola> getKontrola() {
		return kontrola;
	}

	public void setKontrola(List<Kontrola> kontrola) {
		this.kontrola = kontrola;
	}

}

package pl.bc.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class WynikKontroli {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wynikKontroli_seq_gen")
	@SequenceGenerator(name = "wynikKontroli_seq_gen", sequenceName = "WYNIKKONTROLI_SEQ")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "testKontrolny_id")
	private TestKontrolny testKontrolny;

	@ManyToOne
	@JoinColumn(name = "wynikTestu_id")
	private WynikTestu wynikTestu;

	@ManyToOne
	@JoinColumn(name = "przypadekKontrolny_id")
	private PrzypadekKontrolny przypadekKontrolny;

	
	public WynikKontroli() {
	}
	public WynikKontroli(PrzypadekKontrolny przypadek,TestKontrolny test, WynikTestu wynik ) {
		this.przypadekKontrolny=przypadek;
		this.testKontrolny=test;
		this.wynikTestu=wynik;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TestKontrolny getTestKontrolny() {
		return testKontrolny;
	}

	public void setTestKontrolny(TestKontrolny testKontrolny) {
		this.testKontrolny = testKontrolny;
	}

	public WynikTestu getWynikTestu() {
		return wynikTestu;
	}

	public void setWynikTestu(WynikTestu wynikTestu) {
		this.wynikTestu = wynikTestu;
	}

	public PrzypadekKontrolny getPrzypadekKontrolny() {
		return przypadekKontrolny;
	}

	public void setPrzypadekKontrolny(PrzypadekKontrolny przypadekKontrolny) {
		this.przypadekKontrolny = przypadekKontrolny;
	}


}

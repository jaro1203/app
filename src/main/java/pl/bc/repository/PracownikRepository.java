package pl.bc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.bc.entity.Oddzial;
import pl.bc.entity.Pracownik;
import pl.bc.entity.PrzypadekKontrolny;

public interface PracownikRepository extends JpaRepository<Pracownik, Long>{

	@Query ("select p from Pracownik p where p.oddzial= :oddzial")
	List<Pracownik> findByOddzial(@Param("oddzial")Oddzial wybranyOddzial);

}

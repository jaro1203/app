package pl.bc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.bc.entity.Kontrola;

public interface KontrolaRepository extends JpaRepository<Kontrola, Long> {

}

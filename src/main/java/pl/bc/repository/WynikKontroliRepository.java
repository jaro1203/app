package pl.bc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.bc.entity.WynikKontroli;

public interface WynikKontroliRepository extends JpaRepository<WynikKontroli, Long>{

}

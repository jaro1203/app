package pl.bc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.bc.entity.Oddzial;
import pl.bc.entity.Pracownik;
import pl.bc.entity.TestKontrolny;
import pl.bc.entity.WynikTestu;

public interface WynikTestuRepository extends JpaRepository<WynikTestu, Long> {

	
	@Query ("select w from WynikTestu w "
			+ "join fetch w.testKontrolny t "
			+ "where t.id= :id")
	List<WynikTestu> findByTest(@Param("id") Long id);

}

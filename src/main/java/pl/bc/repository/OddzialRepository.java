package pl.bc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.bc.entity.Oddzial;

public interface OddzialRepository extends JpaRepository<Oddzial, Integer>{

}

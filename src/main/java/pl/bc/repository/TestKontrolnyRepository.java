package pl.bc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.bc.entity.PrzypadekKontrolny;
import pl.bc.entity.TestKontrolny;

public interface TestKontrolnyRepository extends JpaRepository<TestKontrolny, Long> {

	@Query("select t from TestKontrolny t " 
			+ "join fetch t.rodzajKontroli rk "
			+ "join fetch rk.kontrola k "
			+ "where k.id= :id" )
	List<TestKontrolny> getTestyKontrolneByIdKontroli(@Param("id") Long id);

}

package pl.bc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.bc.entity.RodzajKontroli;


public interface RodzajKontroliRepository extends JpaRepository<RodzajKontroli, Long>{
	List<RodzajKontroli> findByCzyAktywnaTrue();
}

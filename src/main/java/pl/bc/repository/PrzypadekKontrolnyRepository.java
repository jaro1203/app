package pl.bc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.bc.entity.PrzypadekKontrolny;

public interface PrzypadekKontrolnyRepository extends JpaRepository<PrzypadekKontrolny, Long> {

	@Query ("select p from PrzypadekKontrolny p join p.kontrola k "
			+"where k.id= :id")
	List<PrzypadekKontrolny> findPrzypadkiByKontrola(@Param("id")Long id);

}

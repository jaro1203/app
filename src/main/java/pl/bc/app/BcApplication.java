package pl.bc.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import pl.bc.repository.RodzajKontroliRepository;

@SpringBootApplication (scanBasePackages={
		"pl.bc.entity", "pl.bc.controllers", "pl.bc.repository",
		"pl.bc.services"})
@EntityScan(basePackages = { "pl.bc.entity" })
@EnableJpaRepositories (basePackageClasses = RodzajKontroliRepository.class)
@EnableConfigurationProperties
//@ComponentScan(basePackages= {"pl.bc.controllers"})
public class BcApplication {

	public static void main(String[] args) {
		SpringApplication.run(BcApplication.class, args);
	}

}

package pl.bc.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.bc.entity.Kontrola;
import pl.bc.repository.KontrolaRepository;

@Service
public class KontrolaService {
	@Autowired
	KontrolaRepository kontrolaRepository;

	public List<Kontrola> getKontrole() {
		List<Kontrola> kontrole = kontrolaRepository.findAll();
		return kontrole;
	}
	
	public Optional <Kontrola> getKontrolaById(Long id) {
		return kontrolaRepository.findById(id);

	}
}
